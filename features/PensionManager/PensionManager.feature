Feature: Verifying the security scenarios

  @Security @BackButton
  Scenario: Verifying back button scenario after logout
    Given User logins to Pension manager application
    When  User clicks on logout button and clicks on back button
    Then  User should not be allowed to go inside the application again

  @Security @ErrorCode
  Scenario Outline: Verifying error code scenario
    Given User reaches Pension Manager login page
    When  User enters incorrect "<username1>" and "<password1>"
    Then  Generic error message should be displayed

    Examples:
    |username1    |password1    |
    |testing333063|incorrect    |
    |incorrect    |test0001     |
    |incorrect    |incorrect    |

  @Security @SessionTimeout
  Scenario: Verifying session timeout scenario
    Given User logins to Pension manager application and clicks on logout
    When  User reaches login page again and enter home page URL in browser
    Then  User should remain in Login page


  @Security  @SecureConnection
  Scenario: Verifying http secure connection
    Given Enter HTTP  Login URL for Pension Manager
    When  Login Page should load successfully
    Then  HTTP URL should be redirected to HTTPS URL

  @Security  @DefaultCredential
  Scenario: Verifying default Credential scenario
    Given User reaches Pension Manager login page for verifying default Credential
    When  User enters default username1 and password1 for any ppt
    Then  Generic error message should be displayed for entering default credential

  @Security  @WeakLockOut
   Scenario: Verifying weak lockout mechanism scenario
     Given User reaches Pension Manager login page for verifying weak lockout mechanism
     When  User enters incorrect username1 and password1 for ppt for 5 times
     Then  Lockout error message should be displayed for exceeding

  @Security @SQLInjection
      Scenario: Verifying sqlInjection scenario
       Given User reaches Pension Manager login page for verifying sql Injection
       When  User enters correct username and SQL query which evaluate to true as a password
       Then  User should not be allowed to login



  @Security  @ClickJacking
       Scenario: Verifying Click Jacking scenario
         Given User Opens the HTML file stored for click jacking
         When  User should not able to access any element inside the iframe via click jacking
         Then  Browser window should be closed

  @Security  @BannerGrabbing
         Scenario: Verifying Banner Grabbing scenario
           Given User reaches Pension Manager login page for banner grabbing
           When  User verifies the Response status code from response header section
           Then  Browser should be closed


#  @Security @CrossSiteScripting
#  Scenario: Verifying cross site scripting scenario
#    Given User reaches Pension Manager login page and enters credentials
#    When  User Navigate to Wealth tab and to Personal information page
#    Then  User Enters XSS query in input field and XSS query is not allowed.
