var data = require('../../TestResources/PensionManagerData.js');
var Objects = require(__dirname + '/../../repository/PensionManagerPages.js');
// var action = require(__dirname + '/../../features/step_definitions/ReusableTests.js');
// var ks = require('node-key-sender');
// //var path = require('../../Downloads');
// var fs = require('fs');
// //var email = require(__dirname + '/../../node_modules/emailjs');

var LoginPage, DashboardPage,ForgotPage, HomePageURL;

function initializePageObjects(browser, callback) {
    var PMPage = browser.page.PensionManagerPages();
    LoginPage = PMPage.section.LoginPagePM;
    DashboardPage= PMPage.section.DashboardPagePM;
    ForgotPage= PMPage.section.ForgotPSWDPagePM;
    callback();
}

// function DisplayValue(browser, Xpath, callback)
// {
//
//     browser.useXpath()
//  x= browser.getText(xpath)
//     console.log(x);
//      callback();
// }

module.exports = function()
{
    this.Given(/^User logins to Pension manager application$/, function ()
    {


        browser = this;
        initializePageObjects(browser, function () {
            browser.maximizeWindow()
                .deleteCookies()
                .url(data.PMLoginURL);
            browser.timeoutsImplicitWait(30000);
            LoginPage.waitForElementVisible('@Username', data.wait)
                .setValue('@Username', data.username);
            LoginPage.waitForElementVisible('@Password', data.wait)
                .setValue('@Password', data.password);
            LoginPage.click('@LoginButton')


        })
    });


    this.When(/^User clicks on logout button and clicks on back button$/, function ()
        {

            browser = this;
            DashboardPage.assert.elementPresent('@HomeTab');
            DashboardPage.waitForElementVisible('@LogoutLink', data.wait)
                .click('@LogoutLink');
            browser.pause(1000);
            browser.acceptAlert();
            LoginPage.assert.elementPresent('@LogOutWarning');
            browser.pause(2000);
            browser.back();
            browser.pause(2000)

        });

      this.Then(/^User should not be allowed to go inside the application again$/,function ()
      {

          browser = this;

              //DashboardPage.assert.elementNotPresent('@HomeTab')  // ask subhajit for browser.assert and also how to use callback
               LoginPage.assert.urlContains("Login.tpz");

              // browser.pause(2000)

              console.log('Login page is reached again')

      });

    this.Given(/^User reaches Pension Manager login page$/, function ()
    {


        browser = this;
        initializePageObjects(browser, function ()
        {
            browser.maximizeWindow()
                .deleteCookies()
                .url(data.PMLoginURL);
            browser.timeoutsImplicitWait(30000)
        })
    });

    this.When(/^User enters incorrect "([^"]*)" and "([^"]*)"$/, function (username,password)
    {
        browser=this;
        LoginPage.setValue('@Username',username);
        LoginPage.setValue('@Password',password);
        LoginPage.click('@LoginButton');
        browser.pause(2000)


    });

    this.Then(/^Generic error message should be displayed$/,function ()
    {
        browser=this;
        LoginPage.assert.elementPresent('@UnPwWarning');
               console.log("success");
    });
    this.Given(/^User logins to Pension manager application and clicks on logout$/, function ()
    {


        browser = this;
        initializePageObjects(browser, function ()
        {
            browser.maximizeWindow()
                .deleteCookies()
                .url(data.PMLoginURL);
            browser.timeoutsImplicitWait(30000);
            LoginPage.setValue('@Username',data.username);
            LoginPage.setValue('@Password',data.password);
            LoginPage.click('@LoginButton');
            DashboardPage.assert.elementPresent('@HomeTab');

            browser.url(function(result)    //copying current URL
            {
                 console.log(result.value);
                  HomePageURL=result.value;

            });


            DashboardPage.waitForElementVisible('@LogoutLink', data.wait)
                .click('@LogoutLink');
            browser.pause(1000);
            browser.acceptAlert()

        })
    });

    this.When(/^User reaches login page again and enter home page URL in browser$/, function ()
    {
        browser=this;
        LoginPage.assert.elementPresent('@LogOutWarning');
        browser.pause(2000);
        browser.url(HomePageURL);



    });

    this.Then(/^User should remain in Login page$/,function ()
    {
        browser=this;
        browser.pause(1000);
        LoginPage.assert.elementPresent('@Username');
    });


    this.Given(/^Enter HTTP  Login URL for Pension Manager$/, function ()
    {


        browser = this;
        initializePageObjects(browser, function ()
        {
            browser.maximizeWindow()
                .deleteCookies()
                .url(data.HttpURL);
            browser.timeoutsImplicitWait(30000)

        })
    });
    this.When(/^Login Page should load successfully$/, function()
        {

            LoginPage.assert.elementPresent('@Username');
        });

    this.Then(/^HTTP URL should be redirected to HTTPS URL$/,function()
    {
        browser=this;

        browser.assert.urlContains('https');


    });


    this.Given(/^User reaches Pension Manager login page for verifying default Credential$/, function ()
    {
        browser=this;
        initializePageObjects(browser, function () {
            browser.maximizeWindow()
                .deleteCookies()
                .url(data.PMLoginURL);
            browser.timeoutsImplicitWait(30000)

        })


    });

    this.When(/^User enters default username1 and password1 for any ppt$/, function()
    {
        browser=this;
        LoginPage.setValue('@Username', data.defaultusername);
        browser.pause(1000);
        LoginPage.setValue('@Password', data.defaultpassword);
        browser.pause(1000);
        LoginPage.click('@LoginButton');
    });

    this.Then(/^Generic error message should be displayed for entering default credential$/, function()
    {

        browser=this;
        browser.pause(1000);
        LoginPage.assert.elementPresent('@UnPwWarning')


    });

    this.Given(/^User reaches Pension Manager login page for verifying weak lockout mechanism$/, function ()
    {
        browser=this;
        initializePageObjects(browser, function ()
        {
            browser.maximizeWindow()
                .deleteCookies()
                .url(data.PMLoginURL);
            browser.timeoutsImplicitWait(30000);

        })

    });


    this.When(/^User enters incorrect username1 and password1 for ppt for 5 times$/, function()
    {

        browser=this;
        for(i=1;i<=5;i++)
        {
            LoginPage.setValue('@Username', data.lockoutusername);
            browser.pause(1000);
            LoginPage.setValue('@Password', 'incorrect');
            browser.pause(1000);
            LoginPage.click('@LoginButton');

            if(i==3)
            {
                browser.pause(5000);
                ForgotPage.click('@CancleButton');
                browser.pause(1000);
                browser.acceptAlert();
            }
        }


    })

    this.Then(/^Lockout error message should be displayed for exceeding$/, function()
    {
        browser=this;
        LoginPage.assert.elementPresent('@lockOutWarning');
        browser.pause(1000);
    })


    this.Given(/^User reaches Pension Manager login page for verifying sql Injection$/, function()
    {
        browser=this;
        initializePageObjects(browser, function ()
        {
            browser.maximizeWindow()
                .deleteCookies()
                .url(data.PMLoginURL);
            browser.timeoutsImplicitWait(30000);

        })

    })
    this.When(/^User enters correct username and SQL query which evaluate to true as a password$/, function()
    {
        LoginPage.setValue('@Username', data.lockoutusername);
        browser.pause(1000);
        LoginPage.setValue('@Password', data.sqlpassword);
        browser.pause(1000);
        LoginPage.click('@LoginButton');
        browser.pause(1000);
    })
    this.Then(/^User should not be allowed to login$/, function()
    {

        browser=this;
        LoginPage.assert.elementPresent('@Username');
        browser.pause(1000);

    })

    this.Given(/^User reaches Pension Manager login page and enters credentials$/, function()
    {

        browser=this;
        initializePageObjects(browser, function ()
        {
            browser.maximizeWindow()
                .deleteCookies()
                .url(data.PMLoginURL);
            browser.timeoutsImplicitWait(30000);
            LoginPage.setValue('@Username', data.username);
            browser.pause(1000);
            LoginPage.setValue('@Password', data.password);
            browser.pause(1000);
            LoginPage.click('@LoginButton');
            browser.pause(1000);




           //  DashboardPage.click('@LoginManagement')
           //  browser.pause(1000);
           //  browser.useXpath().click('//a[contains(text(), \'Change your User Name\')]')
           // // browser.useXpath.clearValue
           //  browser.pause(1000);
           //  browser.useXpath().clearValue('//input[@type="text" and @maxlength="32"]')
           //      .pause(1000)
           //      .setValue()

        })

    })


    this.Given(/^User Opens the HTML file stored for click jacking$/, function()
    {

        browser=this;
        initializePageObjects(browser, function ()
        {
            browser.maximizeWindow()
                .deleteCookies()
                .url(data.jackingURL);
            browser.timeoutsImplicitWait(30000);



        })
    })

    this.When(/^User should not able to access any element inside the iframe via click jacking$/, function()
    {
        LoginPage.assert.elementNotPresent('@Username');
    })
    this.Then(/^Browser window should be closed$/,function()
    {
        browser=this;
        browser.end();

    })


    this.Given(/^User reaches Pension Manager login page for banner grabbing$/, function()
    {
        browser=this;
        initializePageObjects(browser, function ()
        {
            browser.maximizeWindow()
                .deleteCookies()
                .url(data.PMLoginURL);
            browser.timeoutsImplicitWait(30000);

        })
    })

    this.When(/^User verifies the Response status code from response header section$/, function()
    {
        browser=this;
        var URL= data.PMLoginURL;
        var request = require('request');

        request(URL, function (error, response, body)
        {
            browser.assert.equal(response.statusCode, 200, "200 OK");
            console.log(response.statusCode);
        });

    })
    this.Then(/^Browser should be closed$/, function()
    {
        browser=this;
        browser.end();

    })

};