
module.exports = {
    sections: {
        LoginPagePM: {
            selector: 'body',
            elements: {
               // SearchBox: {selector: "input[title='Search']"}, css syntax
                Username: {locateStrategy: 'xpath',selector: "//input[@id='usernameId']"},
                Password: {locateStrategy: 'xpath',selector: "//input[@id='passwordId']"},
                LoginButton: {locateStrategy: 'xpath',selector: "//input[@type='submit' and @value='Submit »']"},
                LogOutWarning: {locateStrategy: 'xpath',selector:"//td[contains(text(),'You have now logged out.')]"},
                UnPwWarning:{locateStrategy: 'xpath',selector:"//td[contains(text(),'The User Name and Password entered is not valid.')]"},
                lockOutWarning:{locateStrategy: 'xpath',selector:"//td[contains(text(),'You have exceeded the maximum number of login attempts')]"}

            }
        },

        DashboardPagePM:
            {
            selector: 'body',
            elements:
                {
                // SearchBox: {selector: "input[title='Search']"}, css syntax
                LogoutLink: {locateStrategy: 'xpath',selector: "//*[contains(text(),'Log Out')]"},
                HomeTab: {locateStrategy: 'xpath',selector: "//span[contains(text(),'Home')]"},
                LoginManagement:{locateStrategy: 'xpath',selector: "//span[contains(text(),'Login Management')]"}



                }
        },

        ForgotPSWDPagePM:
            {
                selector: 'body',
                elements:
                    {
                        // SearchBox: {selector: "input[title='Search']"}, css syntax
                        CancleButton: {locateStrategy: 'xpath',selector: "//input[@type='button' and @value='Cancel']"}




                    }
            }



    }

};